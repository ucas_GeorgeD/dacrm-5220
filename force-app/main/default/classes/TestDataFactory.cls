public class TestDataFactory {
    private Integer numberOfAccounts = 10;
    private Integer numberOfOpportunities = 20;
    private Integer numberOfCampaigns = 50;
    private Integer numberOfJobs = 100;
    
    private Account[] accounts;
    private Opportunity[] opportunities;
    private Media_Campaign__c[] campaigns;
    private Campaign_Job__c[] jobs;
    private string[] jobStages;
    private string[] jobTypes;
    
    public TestDataFactory withNumberOfAccounts(Integer numberOfAccounts) {
        this.numberOfAccounts = numberOfAccounts;
        return this;
    }
    
    public TestDataFactory withNumberOfOpportunities(Integer numberOfOpportunities) {
        this.numberOfOpportunities = numberOfOpportunities;
        return this;
    }
    
    public TestDataFactory withNumberOfCampaigns(Integer numberOfCampaigns) {
        this.numberOfCampaigns = numberOfCampaigns;
        return this;
    }
    
    public TestDataFactory withNumberOfJobs(Integer numberOfJobs) {
        this.numberOfJobs = numberOfJobs;
        return this;
    }
    
    public void createTestData() {
        truncateTables();
        createAccounts();
        createOpportunities();
        createCampaigns();
        createJobs();
    }
    
    private void truncateTables() {
        delete [SELECT Id FROM Campaign_Job__c];
        delete [SELECT Id FROM Media_Campaign__c];
        delete [SELECT Id FROM Opportunity];
        delete [SELECT Id FROM Account];
    }
    
    private void createAccounts() {
		accounts = new Account[numberOfAccounts];
        for (Integer i = 0; i < numberOfAccounts; i++) {
            accounts[i] = new Account(Name = 'Test Account ' + i);
        }
        insert accounts;
    }
    
    private void createOpportunities() {
        opportunities = new Opportunity[numberOfOpportunities];
        for (Integer i = 0; i < numberOfOpportunities; i++) {
            opportunities[i] = new Opportunity(Name = 'Test Opportunity ' + i,
                                               AccountId = getRandomAccountId(),
                                               StageName = 'New',
                                               CloseDate = Date.today());
        }
        insert opportunities;
    }
        
    private void createCampaigns() {
        campaigns = new Media_Campaign__c[numberOfCampaigns];
        for (Integer i = 0; i < numberOfCampaigns; i++) {
            campaigns[i] = new Media_Campaign__c(Name = 'Test Campaign ' + i,
                                                 Opportunity__c = getRandomOpportunityId());
        }
        insert campaigns;
    }
    
    private void createJobs() {
        jobs = new Campaign_Job__c[numberOfJobs];
        jobStages = getPicklistValues(Campaign_Job__c.Stage__c);
        jobTypes = getPicklistValues(Campaign_Job__c.Type__c);
        for (Integer i = 0; i < numberOfJobs; i++) {
            Integer campaignIndex = (Integer) Math.floor(Math.random() * campaigns.size());
            jobs[i] = new Campaign_Job__c(Name = 'Test Job ' + i,
                                          Media_Campaign__c = getRandomCampaignId(),
                                          Stage__c = getRandomJobStage(),
                                          Type__c = getRandomJobType());
        }
        insert jobs;
    }
    
    private string[] getPicklistValues(SObjectField field) {
        Schema.DescribeFieldResult fieldDescribe = field.getDescribe();
        List<Schema.PicklistEntry> picklistEntries = fieldDescribe.getPicklistValues();
        string[] picklistValues = new string[picklistEntries.size()];
        for (Integer i = 0; i < picklistEntries.size(); i++) {
            picklistvalues[i] = picklistEntries[i].getLabel();
        }
        return picklistValues;
    }
        
    private Id getRandomAccountId() {
        Integer accountIndex = getRandomInteger(accounts.size());
        return accounts[accountIndex].Id;
    }
    
    private Id getRandomOpportunityId() {
        Integer opportunityIndex = getRandomInteger(opportunities.size());
        return opportunities[opportunityIndex].Id;
    }
    
    private Id getRandomCampaignId() {
        Integer campaignIndex = getRandomInteger(campaigns.size());
        return campaigns[campaignIndex].Id;
    }
    
    private string getRandomJobStage() {
        Integer stageIndex = getRandomInteger(jobStages.size());
        return jobStages[stageIndex];
    }

    private string getRandomJobType() {
        Integer typeIndex = getRandomInteger(jobTypes.size());
        return jobTypes[typeIndex];
    }
    
    private Integer getRandomInteger(Integer maxValue) {
        return (Integer) Math.floor(Math.random() * maxValue);
    }
}