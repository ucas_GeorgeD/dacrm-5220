# DACRM-5220
This repo was created as part of a spike to explore how list views and reports can be used to give Media users visibility of Media Campaigns and Campaign Jobs as part of the AdOps project. It includes code to create test data.

# Setup
1. Clone this repository with git clone https://ucas_GeorgeD@bitbucket.org/ucas_GeorgeD/dacrm-5220.git
2. Change to the project directory with 'cd DACRM-5220'
3. Create a new Scratch Org with 'sfdx force:org:create -a DACRM-5220 -f config/project-scratch-def.json'
4. Push source to your scratch org with sfdx force:source:push -u DACRM-5220

# Test Data
1. Open the developer console
2. Open the execute anonymous window (CTRL + E)
3. Execute the statement 'new TestDataFactory().createTestData();'

# Contents
The repo contains a simplified version of the AdOps data model, with the Account, Opportunity, Media Campaign, Campaign Job hierarchy and a small subset of the fields on those objects. There are a few list views and reports for Media Campaigns and Campaign Jobs. All the relevant tabs are on the Sales app.